from django.db import models

from main.models import LinkType, SubmissionStatus, Subdomain, Cause


class GroupType(models.Model):
    name = models.CharField(max_length=50)
    display_name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name


class GroupScope(models.Model):
    name = models.CharField(max_length=50)
    display_name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

class GroupTag(models.Model):
    display_name = models.CharField(max_length=50)
    path_name = models.CharField(max_length=50)
    parent_tag = models.ForeignKey('self', on_delete=models.PROTECT, null=True, blank=True, related_name='child_tags')
    
    def __str__(self):
        return self.display_name


class Group(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=500, blank=True)
    type = models.ForeignKey(GroupType, on_delete=models.PROTECT, related_name='groups')
    scope = models.ForeignKey(GroupScope, on_delete=models.PROTECT, related_name='groups')
    url = models.CharField(max_length=200, blank=True)
    email = models.CharField(max_length=200, blank=True)
    address = models.CharField(blank=True, max_length=200)
    tags = models.ManyToManyField(GroupTag, related_name="groups", blank=True)
    causes = models.ManyToManyField(Cause, related_name='groups')
    subdomains = models.ManyToManyField(Subdomain, related_name="groups", blank=True)
    submission_status = models.ForeignKey(SubmissionStatus, on_delete=models.PROTECT, related_name="groups")

    
    def sorted_links(self):
        return self.links.order_by('type__name')
    
    def major_link(self):
        link = self.links.filter(type__name='website').first()
        if link:
            return link
        return self.links.order_by('type__name').first()
    
    def __str__(self):
        return self.submission_status.name + ' - ' + self.name
        
        
    
    class Meta:
        ordering = ['name']


class GroupLink(models.Model):
    group = models.ForeignKey(Group, on_delete=models.PROTECT, related_name="links")
    type = models.ForeignKey(LinkType, on_delete=models.PROTECT, related_name="group_links")
    data = models.CharField(max_length=200)
    
    def __str__(self):
        return self.group.name + ' - ' + self.type.name + ' - ' + self.data
    
    def get_full_url(self):
        if self.type.name == 'phone':
            return 'tel:1-' + self.data
        elif self.type.name == 'email':
            return 'mailto:' + self.data
        return self.data
    



