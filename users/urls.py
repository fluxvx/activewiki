
from django.urls import path

from . import views

app_name = 'users'
urlpatterns = [
    path('login/', views.login_page, name='login'),
    path('register_user/', views.register_user, name='register_user'),
    path('login_user/', views.login_user, name='login_user'),
    path('logout_user/', views.logout_user, name='logout_user'),
    path('home/', views.home, name='home'),
    path('newsletter/', views.user_newsletter, name='user_newsletter'),
    path('newsletter/<str:code>/', views.user_newsletter, name='user_newsletter_code'),
    path('send_newsletter/', views.send_newsletter, name='send_newsletter')
]

