from django.contrib import admin

from .models import Fact, FactTag, FactSource

from main.models import Tag
from django.forms import TextInput, Textarea
from django.db import models

class FactAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size':'100'})},
        models.TextField: {'widget': Textarea(attrs={'rows':4, 'cols':40})},
    }
    filter_horizontal = ('sources',)

admin.site.register(Fact, FactAdmin)
admin.site.register(FactTag)
admin.site.register(FactSource)



