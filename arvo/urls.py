
from django.urls import path

from . import views

app_name = 'arvo'
urlpatterns = [
    path('', views.index, name='index'),
    path('facts/', views.facts, name='facts'),
    path('facts/<str:tag_name>/', views.facts, name='facts'),
    path('responses/', views.responses, name='responses'),
    path('resources/', views.resources, name='resources'),
    path('codeofconduct/', views.code_of_conduct, name='code_of_conduct'),
    path('about/', views.about, name='about')
]