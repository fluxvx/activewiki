from django.shortcuts import render

from .models import ArvoFactReference, ArvoResponse
from facts.models import Fact
from main.models import Tag

def index(request):
    return render(request, 'arvo/index.html', {})

def code_of_conduct(request):
    return render(request, 'arvo/code_of_conduct.html', {})


def facts(request, tag_name=''):
    fact_reference = ArvoFactReference.objects.filter(name=tag_name).first()
    child_references = None
    facts = None
    if fact_reference:
        child_references = fact_reference.children.filter(fact__isnull=True)
        facts = [fr.fact for fr in fact_reference.children.filter(fact__isnull=False)]
    else:
        child_references = ArvoFactReference.objects.filter(fact__isnull=True, parent__isnull=True)
        facts = []
    
    context = {
        'tag_name': tag_name,
        'child_references': child_references,
        'facts': facts
    }
    return render(request, 'arvo/facts.html', context)
    
    

def responses(request):
    context = {
        'responses': ArvoResponse.objects.order_by('name')
    }
    return render(request, 'arvo/responses.html', context)
    
def about(request):
    return render(request, 'arvo/about.html', {})


def resources(request):
    return render(request, 'arvo/resources.html', {})