from django.contrib import admin

from .models import Resource, ResourceTag #, ResourceType


admin.site.register(Resource)
admin.site.register(ResourceTag)
# admin.site.register(ResourceType)