from django.urls import path

from . import views

app_name = 'events'
urlpatterns = [
    path('', views.index, name='index'),
    # path('new/', views.new_event, name='new_event'),
    # path('edit/<int:event_id>/', views.edit_event, name='edit'),
    path('load_json/', views.load_json, name='load_json'),
    path('weekly_text_post/', views.weekly_text_post, name='weekly_text_post'),
    path('submit/', views.submit, name='submit'),
    path('<int:event_id>/', views.detail, name='detail'),
    path('<str:paths>/', views.index, name='paths')
]
