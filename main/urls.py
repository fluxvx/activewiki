
from django.urls import path

from . import views
import users.views

app_name = 'main'
urlpatterns = [
    path('', views.index, name='index'),
    path('govegan/', views.govegan, name='govegan'),
    path('getactive/', views.getactive, name='getactive'),
    path('about/', views.about, name='about'),
    path('vegen/', views.vegen, name='vegen'),
    path('newsletter/', users.views.user_newsletter, name='newsletter'),
    path('aapdx/', views.aapdx, name='aapdx'),
    path('<str:path1>/', views.other, name='other'),
    path('<str:path1>/<str:path2>/', views.other, name='other'),
]

