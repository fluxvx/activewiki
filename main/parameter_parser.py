




import requests


from activewiki import secrets

import re

from main.models import Subdomain, Cause



def get_subdomain(meta):
    subdomain = meta['HTTP_HOST'].split('.')[0]
    if subdomain == 'activewiki':
        return None
    return Subdomain.objects.filter(subdomain=subdomain).first()


def get_integer_param(params, name, default):
    if name not in params:
        return default
    try:
        value = int(params[name])
        return value
    except ValueError:
        return default

def get_float_param(params, name, default):
    if name not in params:
        return default
    try:
        value = float(params[name])
        return value
    except ValueError:
        return default

def get_date_param(params, name, default):
    # if the key is not in the dictionary, return the default
    if name not in params:
        return default
    date = params[name]
    if date == '':
        return default
    try:
        date = datetime.datetime.strptime(date, '%Y-%m-%d')
        # r = pytz.timezone('US/Pacific').localize(r)
    except ValueError:
        return default
    return date


def get_tag_param(paths, TagModel):
    for path in paths:
        print(path)
        tag = TagModel.objects.filter(path_name=path).first()
        if tag is not None:
            return tag
    return None

def get_cause_param(paths):
    for path in paths:
        cause = Cause.objects.filter(name=path).first()
        if cause is not None:
            return cause
    return None



def parse_parameters(meta, params, paths, TagModel):
    subdomain = get_subdomain(meta)
    page = get_integer_param(params, 'page', 1)
    if page <= 0:
        return 1
    start_date = get_date_param(params, 'start_date', None)
    end_date = get_date_param(params, 'end_date', None)
    paths = paths.split('+')
    tag = None if TagModel is None else get_tag_param(paths, TagModel)
    cause = get_cause_param(paths)

    return {
        'subdomain': subdomain,
        'page': page,
        'start_date': start_date,
        'end_date': end_date,
        'tag': tag,
        'cause': cause
    }


